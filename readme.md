# @mojoio/qonto
an unofficial api package for the qonto.com API

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@mojoio/qonto)
* [gitlab.com (source)](https://gitlab.com/mojoio/qonto)
* [github.com (source mirror)](https://github.com/mojoio/qonto)
* [docs (typedoc)](https://mojoio.gitlab.io/qonto/)

## Status for master

Status Category | Status Badge
-- | --
GitLab Pipelines | [![pipeline status](https://gitlab.com/mojoio/qonto/badges/master/pipeline.svg)](https://lossless.cloud)
GitLab Pipline Test Coverage | [![coverage report](https://gitlab.com/mojoio/qonto/badges/master/coverage.svg)](https://lossless.cloud)
npm | [![npm downloads per month](https://badgen.net/npm/dy/@mojoio/qonto)](https://lossless.cloud)
Snyk | [![Known Vulnerabilities](https://badgen.net/snyk/mojoio/qonto)](https://lossless.cloud)
TypeScript Support | [![TypeScript](https://badgen.net/badge/TypeScript/>=%203.x/blue?icon=typescript)](https://lossless.cloud)
node Support | [![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
Code Style | [![Code Style](https://badgen.net/badge/style/prettier/purple)](https://lossless.cloud)
PackagePhobia (total standalone install weight) | [![PackagePhobia](https://badgen.net/packagephobia/install/@mojoio/qonto)](https://lossless.cloud)
PackagePhobia (package size on registry) | [![PackagePhobia](https://badgen.net/packagephobia/publish/@mojoio/qonto)](https://lossless.cloud)
BundlePhobia (total size when bundled) | [![BundlePhobia](https://badgen.net/bundlephobia/minzip/@mojoio/qonto)](https://lossless.cloud)
Platform support | [![Supports Windows 10](https://badgen.net/badge/supports%20Windows%2010/yes/green?icon=windows)](https://lossless.cloud) [![Supports Mac OS X](https://badgen.net/badge/supports%20Mac%20OS%20X/yes/green?icon=apple)](https://lossless.cloud)

## Usage

Use TypeScript for best in class intellisense

```typescript
// this is the original test file, which is automatically updated
// test setup
import { expect, tap } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';

const testQenv = new Qenv('./', './.nogit');

// here it gets interesting about how to use the lib
import * as qonto from '../ts/index';

let testQontoAccount: qonto.QontoAccount;

tap.test('should create a qonto account', async () => {
  testQontoAccount = new qonto.QontoAccount({
    qontoUser: testQenv.getEnvVarOnDemand('QONTO_USER'),
    qontoToken: testQenv.getEnvVarOnDemand('QONTO_TOKEN')
  });
  expect(testQontoAccount).to.be.instanceOf(qonto.QontoAccount);
});

tap.test('should get qonto bank account in a qonto account', async () => {
  const bankAccounts = await testQontoAccount.getBankAccounts();
  const bankAccount = bankAccounts[0]
  expect(bankAccount).to.be.instanceOf(qonto.QontoBankAccount);
});

tap.test('should get transactions from a qonto bank account', async () => {
  const bankAccounts = await testQontoAccount.getBankAccounts();
  const bankAccount = bankAccounts[0]
  expect(bankAccount).to.be.instanceOf(qonto.QontoBankAccount);

  const transactions = await bankAccount.getTransactions();
  
});

tap.start();
```

## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
