export interface IQontoOrganizaionApiResponse {
  organization: {
    slug: string;
    bank_accounts: {
      slug: string;
      iban: string;
      bic: string;
      currency: string;
      balance: number;
      balance_cents: number;
      authorized_balance: number;
      authorized_balance_cents: number;
    }[];
  };
}

export interface IQontoTransactionApiResponse {
  transactions: {
    /**
     * ID of the transaction (e.g: acme-corp-1111-1-transaction-123) -> required
     */
    transaction_id: string;
    /**
     * Amount of the transaction, in euros  -> required
     */
    amount: number;
    /**
     * Amount of the transaction, in euro cents -> required
     */
    amount_cents: number;
    /**
     * List of attachments' id -> optional
     */
    attachment_ids: string[];
    /**
     * Amount in the local_currency -> required
     */
    local_amount: number;
    /**
     * Amount in cents of the local_currency -> required
     */
    local_amount_cents: number;
    /**
     * Allowed Values: debit, credit -> required
     */
    side: 'debit' | 'credit';
    /**
     * Allowed Values: transfer, card, direct_debit, income, qonto_fee, cheque, recall, swift_income -> required
     */
    operation_type:
      | 'transfer'
      | 'card'
      | 'direct_debit'
      | 'income'
      | 'qonto_fee'
      | 'cheque'
      | 'recall'
      | 'swift_income';
    /**
     * ISO 4217 currency code of the bank account (can only be EUR, currently) -> required
     */
    currency: 'EUR';
    /**
     * ISO 4217 currency code of the bank account (can be any currency) -> required
     */
    local_currency: string;
    /**
     * Counterparty of the transaction (e.g: Amazon) -> required
     */
    label: string;
    /**
     * Date the transaction impacted the balance of the account, format: yyyy-MM-dd'T'HH:mm:ss.SSSZ -> optional
     */
    settled_at: string;
    /**
     * Date at which the transaction impacted the authorized balance of the account, format: yyyy-MM-dd'T'HH:mm:ss.SSSZ -> required
     */
    emitted_at: string;
    /**
     * Date at which the transaction was last updated, format: yyyy-MM-dd'T'HH:mm:ss.SSSZ -> required
     */
    updated_at: string;
    /**
     * Allowed Values: pending, reversed, declined, completed -> required
     */
    status: string;
    /**
     * Memo added by the user on the transaction ->  optional
     */
    note: string;
    /**
     * Message sent along income, transfer, direct_debit and swift_income transactions -> optional
     */
    reference: string;
    /**
     * Amount of VAT filled in on the transaction, in euros -> optional
     */
    vat_amount: number;
    /**
     * Amount of VAT filled in on the transaction, in euro cents -> optional
     */
    vat_amount_cents: number;
    /**
     * -> optional
     */
    vat_rate: number;
    /**
     * ID of the membership who initiated the transaction -> optional
     */
    initiator_id: string;
    /**
     * List of labels' id -> optional
     */
    label_ids: string[];
    /**
     * Indicates if the transaction's attachment was lost (default: false) -> optional
     */
    attachment_lost: boolean;
    /**
     * Indicates if the transaction's attachment is required (default: true) -> optional
     */
    attachment_required: boolean;
  }[];
}
