import * as plugins from './qonto.plugins';
import * as interfaces from './interfaces';
import { QontoAccount } from './qonto.classes.account';
import { QontoTransaction } from './qonto.classes.transaction';

export interface IQontoBankAccountOptions {
  iban: string;
  bic: string;
  currency: 'EUR';
  slug: string;
}

export class QontoBankAccount implements IQontoBankAccountOptions {
  // STATIC
  public static fromApiBankAccountObject(
    qontoAccountRefArg: QontoAccount,
    apiObjectArg: interfaces.IQontoOrganizaionApiResponse['organization']['bank_accounts'][0]
  ) {
    return new QontoBankAccount(qontoAccountRefArg, {
      iban: apiObjectArg.iban,
      bic: apiObjectArg.bic,
      currency: apiObjectArg.currency as 'EUR',
      slug: apiObjectArg.slug,
    });
  }

  // INSTANCE
  public qontoAccountRef: QontoAccount;

  // data
  iban: string;
  bic: string;
  currency: 'EUR';
  slug: string;

  constructor(qontoAccountRef: QontoAccount, optionsArg: IQontoBankAccountOptions) {
    this.qontoAccountRef = qontoAccountRef;
    Object.assign(this, optionsArg);
  }

  public async getTransactions() {
    const apiResponse: interfaces.IQontoTransactionApiResponse = await this.qontoAccountRef.request(
      'GET',
      `/transactions?slug=${this.slug}&iban=${this.iban}`
    );
    const returnTransactions: QontoTransaction[] = [];
    for (const transactionApiObject of apiResponse.transactions) {
      returnTransactions.push(QontoTransaction.fromApiObject(this, transactionApiObject));
    }
    return returnTransactions;
  }
}
