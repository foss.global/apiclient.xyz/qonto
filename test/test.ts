// this is the original test file, which is automatically updated
// test setup
import { expect, tap } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';

const testQenv = new Qenv('./', './.nogit');

// here it gets interesting about how to use the lib
import * as qonto from '../ts/index';

let testQontoAccount: qonto.QontoAccount;

tap.test('should create a qonto account', async () => {
  testQontoAccount = new qonto.QontoAccount({
    qontoUser: testQenv.getEnvVarOnDemand('QONTO_USER'),
    qontoToken: testQenv.getEnvVarOnDemand('QONTO_TOKEN')
  });
  expect(testQontoAccount).to.be.instanceOf(qonto.QontoAccount);
});

tap.test('should get qonto bank account in a qonto account', async () => {
  const bankAccounts = await testQontoAccount.getBankAccounts();
  const bankAccount = bankAccounts[0]
  expect(bankAccount).to.be.instanceOf(qonto.QontoBankAccount);
});

tap.test('should get transactions from a qonto bank account', async () => {
  const bankAccounts = await testQontoAccount.getBankAccounts();
  const bankAccount = bankAccounts[0]
  expect(bankAccount).to.be.instanceOf(qonto.QontoBankAccount);
  const transactions = await bankAccount.getTransactions();
});

tap.start();
